-- Add New Records

-- Add Artists
INSERT INTO artists (name) VALUES ("Taylor Swift");
INSERT INTO artists (name) VALUES ("Lady Gaga");
INSERT INTO artists (name) VALUES ("Justin Bieber");
INSERT INTO artists (name) VALUES ("Arian Grande");
INSERT INTO artists (name) VALUES ("Bruno Mars");

-- Add Albums

INSERT INTO albums (album_title, date_released, artists_id) VALUES (
	"Fearless",
	"2008-01-01",
	3
);
INSERT INTO albums (album_title, date_released, artists_id) VALUES (
	"Red",
	"2012-01-01",
	3
);

INSERT INTO albums (album_title, date_released, artists_id) VALUES (
	"A Star is Born",
	"2018-01-01",
	4
);

INSERT INTO albums (album_title, date_released, artists_id) VALUES (
	"Born This Way",
	"2011-01-01",
	4
);

INSERT INTO albums (album_title, date_released, artists_id) VALUES (
	"Purpose",
	"2015-01-01",
	5
);

INSERT INTO albums (album_title, date_released, artists_id) VALUES (
	"Dangerous Woman",
	"2016-01-01",
	6
);

-- Add Songs
INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Fearless",
	246,
	"Pop rock",
	3
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"State of Grace",
	253,
	"Rock",
	4
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Black Eyes",
	151,
	"Rock",
	5
);

INSERT INTO songs (song_name, length, genre, album_id) VALUES(
	"Born This Way",
	252,
	"Electropop",
	6
);

-- Advanced Selects

-- Exclude records (NOT operator)
SELECT * FROM songs WHERE id !=5;

-- Greater than (or equal to)
SELECT * FROM songs WHERE id >= 4;

-- Less than (or equal to)
SELECT * FROM songs WHERE id <= 6;

-- Get specific IDs (OR)
SELECT * FROM songs WHERE id = 1 OR id = 5 OR id = 6;

-- Get specific IDs (IN)
SELECT * FROM songs WHERE id IN (1, 5, 6);

-- Find partial matches
SELECT * FROM songs WHERE song_name LIKE "%e"; --select keyword from the end

SELECT * FROM songs WHERE song_name LIKE "b%"; --select keyword from the start
SELECT * FROM songs WHERE song_name LIKE "%a%"; --select keyword from the anywhere

-- Sort records
SELECT * FROM songs ORDER BY song_name ASC;
SELECT * FROM songs ORDER BY song_name DESC;

-- Getting distinct records (show all unique values)
SELECT DISTINCT genre FROM songs;

SELECT COUNT (*) FROM songs WHERE genre = "Dancehall";

-- TABLE JOINS
-- Combine artists and albums table (JOIN/INNER JOIN)
SELECT * FROM artists
	JOIN albums ON artists.id = albums.artist_id;

-- LEFT JOIN
SELECT * FROM artists
	LEFT JOIN albums ON artists.id = albums.artists_id;

-- RIGHT JOIN
SELECT * FROM artists
	RIGHT JOIN albums ON artists.id = albums.artist_id;

-- Join multiple tables
SELECT artists.name, albums.albums_title, songs.song_name FROM songs
	JOIN albums ON albums.id = albums.artists_id
	JOIN songs ON albums.id = songs.album_id;

-- Mini Activity:
SELECT songs.song_name, songs.length, albums.album_title, artists.name FROM songs
	JOIN albums ON songs.album_id = albums.id
	JOIN artists ON albums.artist_id = artists.id; WHERE artists.name = "Lady Gaga";
	



